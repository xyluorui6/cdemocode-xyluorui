// echo_eof.c -- 重复输入，直到文件结尾
#include<stdio.h>
int main()
{
    int ch;
    
    // 用getchar()的返回值和EOF作比较，如果两值不同，就说明还没有到达文件结尾。
    while((ch = getchar()) != EOF)
        putchar(ch);

    return 0;
}