/*
    用循环计算矩形的面积，如果用户输入非数字作为矩形的长或宽，则终止循环。
*/

// break.c -- 使用break退出循环
#include<stdio.h>
int mian()
{
    float length,width;

    printf("Enter the length of the rectangle:\n");
    while(scanf("%f",&length) == 1)
    {
        printf("Length = %0.2f:\n",length);
        printf("Enter its width:\n");
        if(scanf("%f",&width) != 1)
            break;
        printf("Width = %0.2f:\n", width);
        printf("Area = %0.2f:\n",length * width);
        printf("Enter the length of the rectangle:\n");
    }
    printf("Done.");

    return 0;
}