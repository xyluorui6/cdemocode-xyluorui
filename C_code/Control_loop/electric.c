//electric.c -- 计算电费
#include<stdio.h>
#define RATE1 0.13230   //首次使用360kwh的费率
#define RATE2 0.15040   //接着再使用108kwh的费率
#define RATE3 0.30025   //接着再使用252kwh的费率
#define RATE4 0.34025   //使用超过720kwh的费率
#define BREAK1 360.0    //费率第一个临界点
#define BREAK2 468.0    //费率第二个临界点
#define BREAK3 720.0    //费率第三个临界点
